package telran;

import telran.util.JoinFilePerformance;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;


public class copyTests {
    private static final String SOURCE_FILE = "\\JAVA_PROJECTS\\116-34-FilePerformance\\src\\golovasheveo";
    private static final String COPY_FILE = "\\JAVA_PROJECTS\\116-34-FilePerformance\\src\\golovasheveocp";

    private static long bufferSize;

    public static void main(String[] args) {
        runStandardTest();
        runExtendedTest();
    }

    private static void runExtendedTest() {

        long[] buffer = {
                100,
                100 * 1024 * 1024,
                Runtime.getRuntime().freeMemory()
        };


        Arrays.stream(buffer)
                .forEach(value -> {
                    bufferSize = value;
                    new JoinFilePerformance(
                            "ExtendedTest, buffer: " + bufferSize,
                            1,
                            copyTests::extendedMethod
                    ).run();
                });
    }

    private static void runStandardTest() {
        new JoinFilePerformance(
                "Standard Files.copy", 1, copyTests::standardMethod
        ).run();
    }

    private static void standardMethod() throws IOException {
        Files.copy(Paths.get(SOURCE_FILE), Paths.get(COPY_FILE), StandardCopyOption.REPLACE_EXISTING);
    }

    private static void extendedMethod() throws IOException {
        Files.deleteIfExists(Paths.get(COPY_FILE));

        InputStream input
                = Files.newInputStream(Paths.get(SOURCE_FILE));
        OutputStream output
                = new FileOutputStream(COPY_FILE);

        int length;
        var buffer = new byte[(int) bufferSize];
        while ((length = input.read(buffer)) != -1) output.write(buffer, 0, length);

        input.close();
        output.close();
    }
}