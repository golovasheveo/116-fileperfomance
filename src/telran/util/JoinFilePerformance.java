package telran.util;

import telran.tests.performance.PerformanceTest;

import java.io.IOException;

public class JoinFilePerformance extends PerformanceTest {
    FileMethod performance;

    public JoinFilePerformance(String testName, Integer nRuns, FileMethod performance) {
        super(testName, nRuns);
        this.performance = performance;
    }

    @Override
    protected void runTest() {
        try {
            performance.run();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}