package telran.util;

import java.io.IOException;

public interface FileMethod {
    void run() throws IOException;
}